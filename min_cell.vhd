------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use ieee.math_real.all;
use work.slice_pkg.all;

entity min_cell is
    generic (
      dwidth : natural := 4;  -- data width
      pwidth : natural := 1); -- pattern width
    port (
      -- data values
      data0  : in  unsigned(dwidth - 1 downto 0);
      data1  : in  unsigned(dwidth - 1 downto 0);
      mdata  : out unsigned(dwidth - 1 downto 0);
      -- minimum bit mask pattern
      patt0  : in  std_logic_vector(pwidth - 1 downto 0);
      patt1  : in  std_logic_vector(pwidth - 1 downto 0);
      mpatt  : out std_logic_vector(2*pwidth - 1 downto 0));
end entity min_cell;

architecture logic of min_cell is

  constant zeropatt : std_logic_vector(pwidth - 1 downto 0) := (others => '0');

  signal eq : boolean;
  signal lt : boolean;

begin -- logic of min_cell

  eq <= (data0 = data1);
  lt <= (data0 < data1);
  
  data_proc: process(eq,lt,data0,data1)
  begin -- data_process
    if eq and not lt then
      -- data0 is equal to data1
      mdata <= data0;
    elsif not eq and lt then
      -- data0 is less than data1
      mdata <= data0;
    elsif not eq and not lt then
      -- data0 is greater than data1
      mdata <= data1;
    else -- this shouldn't happen
      mdata <= (others => '0');
      assert false
        report "Invalid comparison"
        severity error;
    end if;
  end process data_proc;

  patt_proc: process(eq,lt,patt0,patt1)
    variable res : std_logic_vector(mpatt'RANGE);
  begin
    if eq and not lt then 
      -- data0 is equal to data1
      slv_insert_slice(res,patt0,0);
      slv_insert_slice(res,patt1,1);
    elsif not eq and lt then
      -- data0 is less than data1
      slv_insert_slice(res,patt0,0);
      slv_insert_slice(res,zeropatt,1);
    elsif not eq and not lt then
      -- data0 is greater than data1
      slv_insert_slice(res,zeropatt,0);
      slv_insert_slice(res,patt1,1);
    else -- this shouldn't happen
      assert false
        report "Invalid comparison"
        severity error;
    end if;
    mpatt <= res;
  end process patt_proc;
  
end architecture logic; -- of min_cell
