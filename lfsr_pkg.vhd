------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

package lfsr_pkg is
  
  type taps_list_type is array(0 to 3) of natural;
  type feedback_table_type is array(1 to 36) of taps_list_type;

  -- check out http://www.ece.cmu.edu/~koopman/lfsr/ for more polynomial taps.
  constant maximal_length_table : feedback_table_type := (
    (  9,  5,  0,  0),  --  1
    ( 10,  7,  0,  0),  --  2
    ( 11,  9,  0,  0),  --  3
    ( 12,  6,  4,  1),  --  4
    ( 13,  4,  3,  1),  --  5
    ( 15, 14,  0,  0),  --  6
    ( 16, 15, 13,  4),  --  7
    ( 17, 14,  0,  0),  --  8
    ( 18, 11,  0,  0),  --  9
    ( 19,  6,  2,  1),  -- 10
    ( 21, 19,  0,  0),  -- 11
    ( 23, 18,  0,  0),  -- 12
    ( 25, 22,  0,  0),  -- 13
    ( 26,  6,  2,  1),  -- 14
    ( 28, 25,  0,  0),  -- 15
    ( 29, 27,  0,  0),  -- 16
    ( 30,  6,  4,  1),  -- 17
    ( 31, 28,  0,  0),  -- 18
    ( 32, 22,  2,  1),  -- 19
    ( 33, 20,  0,  0),  -- 20
    ( 38,  6,  5,  1),  -- 21
    ( 43, 42, 38, 37),  -- 22
    ( 47, 42,  0,  0),  -- 23
    ( 51, 50, 36, 35),  -- 24
    ( 55, 31,  0,  0),  -- 25
    ( 57, 50,  0,  0),  -- 26
    ( 61, 60, 46, 45),  -- 27
    ( 67, 66, 58, 57),  -- 28
    ( 72, 66, 25, 19),  -- 29
    ( 77, 76, 47, 46),  -- 30
    ( 81, 77,  0,  0),  -- 31
    ( 89, 51,  0,  0),  -- 32
    ( 96, 94, 49, 47),  -- 33
    ( 97, 91,  0,  0),  -- 34
    ( 99, 97, 54, 52),  -- 35
    (100, 63,  0,  0)   -- 36
  );

  function find_taps(w: natural) return natural;

  -- from http://www.asic-world.com/examples/vhdl/package.html
  --   by Alexander H Pham
  function many_to_one_fb(data, taps : std_logic_vector)
    return std_logic_vector; -- Fibonacci LFSR
  function one_to_many_fb(data, taps : std_logic_vector)
    return std_logic_vector; -- Galois LFSR
  
end package lfsr_pkg;

------------------------------------------------------
package body lfsr_pkg is

  function find_taps(w: natural) return natural is
  begin
    for i in maximal_length_table'left to maximal_length_table'right loop
      if w = maximal_length_table(i)(0) then
        return i;
      end if;
    end loop;
    return 0;
  end function find_taps;

  ----------------------------------------------------
  function many_to_one_fb(data, taps : std_logic_vector)
    return std_logic_vector is
    constant tlen     : natural := taps'length;
    variable xor_taps : std_logic;
    variable all_0s   : std_logic;
    variable feedback : std_logic;
  begin -- many_to_one_fb
    assert (data'length >= tlen)
      report "data length must be greater than or equal to taps length"
      severity failure;
    
    -- Validate if lfsr = to zero (Prohibit Value)
    if (DATA(data'length-2 downto 0) = 0) then
      all_0s := '1';
    else
      all_0s := '0';
    end if;
    
    xor_taps := '0';
    for idx in 0 to (tlen - 1) loop
      if (TAPS(idx) = '1') then
        xor_taps := xor_taps xor DATA(idx);
      end if;
    end loop;
    
    feedback := xor_taps xor all_0s;
    
    return DATA((DATA'length-2) downto 0) & feedback;
  end function many_to_one_fb;

  ----------------------------------------------------
  function one_to_many_fb(data, taps : std_logic_vector)
    return std_logic_vector is
    constant width     : natural := data'length;
    variable xor_taps  : std_logic;
    variable all_0s    : std_logic;
    variable feedback  : std_logic;
    variable result    : std_logic_vector(width-1 downto 0);
  begin -- one_to_many_fb
    assert (width >= taps'length)
      report "data length must be greater than or equal to taps length"
      severity failure;
    
    -- Validate if lfsr = to zero (Prohibit Value)
    if (DATA(width-2 downto 0) = 0) then
      all_0s := '1';
    else
      all_0s := '0';
    end if;
    
    feedback := DATA(width-1) xor all_0s;
    
    -- XOR the taps with the feedback
    result(0) := feedback;
    for idx in 0 to (width - 2) loop
      if idx < TAPS'length then
        if (TAPS(idx) = '1') then
          result(idx+1) := feedback xor DATA(idx);
        else
          result(idx+1) := DATA(idx);
        end if;
      end if;
    end loop;

    return result;
  end function one_to_many_fb;
  
end package body lfsr_pkg;
