#!/usr/bin/python
#
# Kiet To - 2014
# Test indexing scheme
#

depth = 3
dwidth = 2**depth;

data0=dict()
data1=dict()
mdata=dict()
patt0=dict()
patt1=dict()
mpatt=dict()

def btree_to_linear(depth,row,col):
    nodelevel = depth + 1
    return 2**nodelevel - 2**(nodelevel-row) + col;

for i in range(depth):
    print("i=%d" % (i))
    for j in range(2**(depth-i-1)):
        pwidth = 2**(depth-i-1)
        print(" j=%d" % (j))
        print("  pwidth=%d" % (pwidth))
        # z = f(a,b)
        data0['index'] = btree_to_linear(depth,i,2*j)   # a
        data1['index'] = btree_to_linear(depth,i,2*j+1) # b
        mdata['index'] = btree_to_linear(depth,i+1,j)   # z

        print('  data0=%d' % (data0['index']))
        print('  data1=%d' % (data1['index']))
        print('  mdata=%d' % (mdata['index']))

        patt0['upper'] = 2**(i)*(2*j)
        patt0['lower'] = 2**(i)*(2*j) + 2**(i) - 1
        print('  patt0[%d:%d]' % (patt0['upper'],patt0['lower']))
        patt1['upper'] = 2**(i)*(2*j+1)
        patt1['lower'] = 2**(i)*(2*j+1) + 2**(i) - 1
        print('  patt1[%d:%d]' % (patt1['upper'],patt1['lower']))
        mpatt['upper'] = 2**(i)*(2*j)
        mpatt['lower'] = 2**(i)*(2*j+1) + 2**(i) - 1
        print('  mpatt[%d][%d:%d]' % (i,mpatt['upper'],mpatt['lower']))
