#!/usr/bin/env python
#
# Donation:
#    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
#    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
#

# program to generate cordic constants for VHDL

from mpmath import *

# cordic precision
width=64
cordic_prec=width+1
cordic_stage=64

# double the internal precision (ad-hoc)
mp.prec=(cordic_prec*2) 

# two's complement signed binary
def to_signed(value,bits=32,decimal=2):
    r = 2**(decimal-1)
    if value >= r or value < -r:
        raise ValueError('value out of range')
    v = value
    if v < 0:
        s = str(1)
        n = -r
    else:
        s = str(0)
        n = 0
    v = (v - n) / (r/2)
    for i in range(bits-1):
        if v >= 1.0:
            s = s+'1'
            v = v - 1.0
        else:
            s = s+'0'
        v = v * 2.0
    return s

angles = list()
kvalues = list()
kvalues.append(1.0)
for i in range(cordic_stage):
    v1 = power(2.0,-i)
    v2 = power(2.0,-2*i)
    angles.append(atan(v1))
    ki = 1.0 / sqrt(1.0 + v2)
    kvalues.append(kvalues[-1] * ki)

del kvalues[0]

package_name = "cordic_pkg"
print("library ieee;")
print("use ieee.std_logic_1164.all;")
print("package %s is\n" % (package_name))

print("constant width : natural := %d;" % (width))
print("-- two's complement requires extra sign bit")
print("constant cordic_precision : natural := width+1;")
print("constant cordic_stages : natural := %d;\n" % (cordic_stage))

print("subtype fix_pt is std_logic_vector(cordic_precision-1 downto 0);")
print("type k_array is array (0 to cordic_stages-1) of fix_pt;")
print("type a_array is array (0 to cordic_stages-1) of fix_pt;\n")

print("-- cordic scaling constant k --");
print("constant cordic_k : k_array := (")
for k in kvalues:
    if k != kvalues[-1]:
        print('  "%s",' % (to_signed(k,cordic_prec,1)))
    else:
        print('  "%s");\n' % (to_signed(k,cordic_prec,1)))

print("-- cordic angles --");
print("constant cordic_a : a_array := (")
for a in angles:
    if a != angles[-1]:
        print('  "%s",' % (to_signed(a,cordic_prec,1)))
    else:
        print('  "%s");\n' % (to_signed(a,cordic_prec,1)))

print("-- pi/2 --");
print('constant pi_2 : fix_pt := "%s";\n' % (to_signed(pi/2,cordic_prec,2)))

print("end package %s;" % (package_name))
