------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use ieee.math_real.all;

use std.textio.all;

use work.slice_pkg.all;

entity min_tree_tb is
end entity min_tree_tb;

architecture testbench of min_tree_tb is

  constant width : natural := 4;
  constant depth : natural := 3;
  
  component min_tree is
    generic (
      width : natural;
      depth : natural);
    port (
      inputs   : in  std_logic_vector((2**depth * width - 1) downto 0);
      minvalue : out unsigned(width-1 downto 0);
      pos      : out std_logic_vector(2**depth - 1 downto 0));
  end component;

  -- configuration definition
  for all: min_tree use entity WORK.min_tree(logic);

  -- test vector patterns
  type natural_vector is array(2**depth - 1 downto 0) of natural;
  type test_pattern is
  record
    v   : natural_vector;
    min : natural;
    pos : natural;
  end record;
  type test_array is array(integer range <>) of test_pattern;
  constant patt : test_array(0 to 3) :=
    (0 => (v => ( 0, 1, 2, 3, 4, 5, 6, 7), min => 0, pos => 128),
     1 => (v => (15,15, 4, 3,14,13, 4, 6), min => 3, pos => 16 ),
     2 => (v => ( 2,10, 6, 2, 4, 2, 2, 0), min => 0, pos => 1  ),
     3 => (v => ( 5, 6, 0, 5, 3,15, 9, 0), min => 0, pos => 33 ));

  function vector_to_slv(v : natural_vector; width: natural)
    return std_logic_vector is
    constant n : natural := v'LENGTH;
    variable res : std_logic_vector(n*width - 1 downto 0);
  begin -- vector_to_slv
    for i in v'range loop
      slv_insert_slice(res,std_logic_vector(to_unsigned(v(i),width)),i);
    end loop;
    return res;
  end function vector_to_slv;
  
  signal values   : std_logic_vector((2**depth * width - 1) downto 0) :=
    (others => '0');
  signal result   : unsigned(width-1 downto 0);
  signal position : std_logic_vector(2**depth - 1 downto 0);

begin -- testbench

  min_tree_0: min_tree
    generic map (
      width => width,
      depth => depth)
    port map (
      inputs   => values,
      minvalue => result,
      pos      => position);

  driver: process
    variable data         : std_logic_vector(values'RANGE);
    variable L            : line;
  begin -- driver
    for i in patt'range loop
      data := vector_to_slv(patt(i).v,width);
      values <= data;
      wait for 10 ns;
      write(L,string'("  values=")); slice_write(L,values,width);
      writeline(output,L);
      write(L,string'("  result=")); write(L,std_logic_vector(result));
      if result = patt(i).min then
        write(L,string'(" [ OK ]"));
      else
        write(L,string'(" [ FAILED ]"));
      end if;
      write(L,string'("; position=")); write(L,position);
      if position = std_logic_vector(
        to_unsigned(patt(i).pos,position'LENGTH)) then
        write(L,string'(" [ OK ]"));
      else
        write(L,string'(" [ FAILED ]"));
      end if;
      writeline(output,L);
    end loop;
    wait;
  end process driver;
  
end architecture testbench;
