------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity clock_gen is
  generic (
    finite    : boolean := true;  -- use finite number of clock pulses
    clk_start : std_logic := '1'; -- starting state of clock  
    period    : time    := 10 ns; -- time period of clock
    clk_tick  : natural := 100;   -- number of clock pulses (if finite)
    rst_tick  : real    := 5.0;   -- number of clock pulses to hold reset
    clk_delay : real    := 0.0);  -- number of clock period before clock start
  port (
    clk_o : out std_logic;
    rst_o : out std_logic);
end entity clock_gen;

architecture behaviour of clock_gen is

  signal iClock : std_logic := clk_start;
  signal iReset : std_logic := '1';
  
begin -- behaviour

  assert (rst_tick < real(clk_tick))
    report "reset will be held high for clock operations."
    severity warning;
  assert (rst_tick >= 0.0)
    report "rst_tick value must be non-negative."
    severity error;
  
  clk_o <= iClock;
  rst_o <= iReset;
  
  finite_clock: if finite generate
    clock_proc: process
    begin -- clock_proc
      iClock <= clk_start;
      wait for (clk_delay * period);
      for i in 1 to clk_tick loop
        wait for (period / 2);
        iClock <= not iClock;
        wait for (period / 2);
        iClock <= not iClock;
      end loop;
      wait; -- simuation stops here
    end process clock_proc;
  end generate finite_clock;

  infinite_clock: if not finite generate
    clock_proc: process
      variable first_time : boolean := true;
    begin -- clock_proc
      if first_time then
        iClock <= clk_start;
        wait for (clk_delay * period);
        first_time := false;
      else
        wait for (period / 2);
        iClock <= not iClock;
        wait for (period / 2);
        iClock <= not iClock;
      end if;
    end process clock_proc;
  end generate infinite_clock;

  reset_proc : process
  begin -- process
    wait for (rst_tick * period);
    iReset <= not iReset;
    wait;
  end process reset_proc;
  
end architecture behaviour;
