#!/usr/bin/env bash
#
# Kiet To
#

if [ ! -d work ]; then
    make
fi

TB=`make list | grep 'ARCHITECTURE testbench' | sed -e 's/^.* OF \(.*\)$/\1/'`
for entity in ${TB}; do
    make ${entity}.run
done

echo "------"
echo "To view results run:"
for entity in ${TB}; do
    if [ -f ${entity}.vcd ]; then
        echo "  gtkwave ${entity}.vcd"
    fi
done
