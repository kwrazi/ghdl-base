------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity file_io_tb is
end entity file_io_tb;

use work.component_lib.all;

architecture testbench of file_io_tb is

  constant width : natural := 16;
  constant ticks : natural := 100;

  for all : clock_gen use entity work.clock_gen(behaviour);
  for all : lfsr_galois use entity work.lfsr_galois(rtl);
  for all : file_log use entity work.file_log(debugging);
  for all : file_src use entity work.file_src(debugging);
  for all : file_cmp use entity work.file_cmp(debugging);

  signal clk    : std_logic;
  signal rst    : std_logic;
  signal seed   : std_logic_vector(width-1 downto 0);
  signal rndout : std_logic_vector(width-1 downto 0);
  signal data1  : std_logic_vector(width-1 downto 0);
  signal data2  : std_logic_vector(width-1 downto 0);

begin -- testbench;

  seed <= (width-1 => '1', others => '0');
  
  clk_gen0: clock_gen
    generic map (
      finite    => true,
      clk_start => '0',
      period    => 10 ns,
      clk_tick  => ticks,
      rst_tick  => 5.0)
    port map (
      clk_o     => clk,
      rst_o     => rst);

  prbg: lfsr_galois
    generic map (
      width  => width)
    port map(
      clk_i  => clk,
      rst_i  => rst,
      load_i => '0',
      ce_i   => '1',
      seed_i => seed,
      q_o    => rndout);
  
  flog: file_log
    generic map (
      log_file => "lfsr_galois.log",
      width    => width)
    port map (
      clk_i  => clk,
      rst_i  => rst,
      data_i => rndout);

  fsrc: file_src
    generic map (
      src_file => "data_bad.dat",
      width    => width)
    port map (
      data_o   => data1);

  fcmp: file_cmp
    generic map (
      src_file => "data_bad.dat",
      width    => width)
    port map (
      clk_i  => clk,
      rst_i  => rst,
      data_i => rndout,
      data_o => data2);
  
end architecture testbench;
