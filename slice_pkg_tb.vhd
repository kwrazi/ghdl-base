------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

use std.textio.all;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use ieee.math_real.all;

use work.slice_pkg.all;

entity slice_pkg_tb is
end entity slice_pkg_tb;

architecture testbench of slice_pkg_tb is

  -- system constants
  constant width : natural := 4;
  constant depth : natural := 3;
  constant btree_size : natural := (2**depth - 1);
  
  constant real_range : real := real(2**width);

  procedure tidy_print(L : inout line; data: std_logic_vector) is
    constant slices : natural := data'LENGTH / width;
    variable slice : std_logic_vector(width-1 downto 0);
  begin
    for i in slices-1 downto 0 loop
      slice := data(((i + 1)*width - 1) downto i*width);
      write(L,slice);
      write(L,' ');
    end loop;
  end procedure tidy_print;
  
begin -- testbench
  test: process
    -- random number generator
    variable seed1, seed2: positive;
    variable rand: real;
    variable int_rand: integer;
    variable stim: std_logic_vector(width-1 downto 0);
    
    variable data  : std_logic_vector((btree_size * width - 1) downto 0);
    variable L     : line;
  begin
    -- create random generator
    uniform(seed1, seed2, rand);
    uniform(seed1, seed2, rand);
    write(L,string'("width=")); write(L,width); writeline(output,L);
    write(L,string'("depth=")); write(L,depth); writeline(output,L);
    -- test linear slices functions and procedures
    write(L,string'("-- test slv_insert_slice -- ")); writeline(output,L);
    write(L,string'("data'LENGTH=")); write(L,data'LENGTH); writeline(output,L);
    write(L,string'("stim'LENGTH=")); write(L,stim'LENGTH); writeline(output,L);
    for i in integer(btree_size - 1) downto 0 loop
      uniform(seed1, seed2, rand);
      int_rand := integer(trunc(rand*real_range));
      stim := std_logic_vector(to_unsigned(int_rand, stim'LENGTH));
      slv_insert_slice(data,stim,i);
      slice_write(L,data,width); writeline(output,L);
    end loop;
    
    write(L,string'("-- test slv_read_slice -- ")); writeline(output,L);
    for i in integer(btree_size - 1) downto 0 loop
      write(L,string'("data["));
      write(L,i);
      write(L,string'("]="));
      write(L,slv_read_slice(data,width,i));
      writeline(output,L);
    end loop;
    
    write(L,string'("-- test btree_to_linear -- ")); writeline(output,L);
    write(L,string'("depth=")); write(L,depth); writeline(output,L);
    for i in 0 to depth-1 loop
      for j in 0 to integer(2**(depth-i-1)-1) loop
        write(L,string'("row=")); write(L,i);
        write(L,string'(";col=")); write(L,j);
        write(L,string'(";index=")); write(L,btree_to_linear(depth,i,j));
        writeline(output,L);
      end loop;
    end loop;
    
    write(L,string'("-- test slv_insert_btree -- ")); writeline(output,L);
    for i in 0 to depth-1 loop
      for j in 0 to integer(2**(depth-i-1)-1) loop
        uniform(seed1, seed2, rand);
        int_rand := integer(trunc(rand*real_range));
        stim := std_logic_vector(to_unsigned(int_rand, stim'LENGTH));
        write(L,string'("write data["));
        write(L,i); write(L,','); write(L,j);
        write(L,string'("]<-")); write(L,int_rand);
        slv_insert_btree(data,stim,depth,i,j);
        writeline(output,L);
      end loop;
    end loop;
    
    write(L,string'("-- test slv_read_btree -- ")); writeline(output,L);
    for i in 0 to depth-1 loop
      for j in 0 to integer(2**(depth-i-1)-1) loop
        write(L,string'("data["));
        write(L,i); write(L,','); write(L,j);
        write(L,string'("]="));
        write(L,slv_read_btree(data,width,depth,i,j));
        writeline(output,L);
      end loop;
    end loop;
    
    wait;
  end process;
  
end architecture testbench; -- of slice_pkg_tb
