------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------
-- Code based on Ing. Ivo Viscor 2000 paper, "Gray counter in VHDL"

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;
use ieee.math_real.all;

package graycode_lib is
  
  component gray_1
    generic(
      reset_value : std_logic);
    port(
      rst_i : in    std_logic;
      clk_i : in    std_logic;
      q_i   : in    std_logic;
      z_i   : in    std_logic;
      q_o   : inout std_logic;
      z_o   : out   std_logic);
  end component gray_1;
  
  component gray_counter
    generic(
      width: natural);
    port(
      rst_i  : in    std_logic;
      clk_i  : in    std_logic;
      p_io   : inout std_logic;
      q_io   : inout std_logic_vector(width-1 downto 0));
  end component gray_counter;

  component gray_to_binary
    generic(
      width: natural);
    port(
      g_i  : in  std_logic_vector(width-1 downto 0);
      b_o  : out std_logic_vector(width-1 downto 0));
  end component gray_to_binary;

  component binary_to_gray
    generic(
      width: natural := 8);
    port(
      b_i  : in  std_logic_vector(width-1 downto 0);
      g_o  : out std_logic_vector(width-1 downto 0));
  end component binary_to_gray;

  component gray_counter_asym
    generic(
      width: natural);
    port(
      rst_i : in    std_logic;
      clk_i : in    std_logic;
      p_io  : inout std_logic;   -- parity bit
      q_io  : inout std_logic_vector(width-1 downto 0));
  end component gray_counter_asym;
  
end package graycode_lib;
