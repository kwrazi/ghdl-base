------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use ieee.math_real.all;

use std.textio.all;

use work.slice_pkg.all;

entity min_cell_tb is
end entity min_cell_tb;

architecture testbench of min_cell_tb is

  component min_cell is
    generic (
      dwidth : natural;  -- data width
      pwidth : natural); -- pattern width
    port (
      -- data values
      data0  : in  unsigned(dwidth - 1 downto 0);
      data1  : in  unsigned(dwidth - 1 downto 0);
      mdata  : out unsigned(dwidth - 1 downto 0);
      -- minimum bit mask pattern
      patt0  : in  std_logic_vector(pwidth - 1 downto 0);
      patt1  : in  std_logic_vector(pwidth - 1 downto 0);
      mpatt  : out std_logic_vector(2*pwidth - 1 downto 0));
  end component min_cell;

  for all: min_cell use entity work.min_cell(logic);

  constant dwidth : natural := 8;
  constant pwidth : natural := 2;

  type test_pattern is
  record
    a: natural;
    b: natural;
    c: natural; -- expected result
    pa: natural;
    pb: natural;
    pc: natural; -- expected result
  end record;

  type test_array is array(integer range <>) of test_pattern;

  constant unsigned_zero : unsigned(dwidth - 1 downto 0) := (others => '0');
  constant zero : std_logic_vector(pwidth - 1 downto 0) := (others => '0');
  
  signal a : unsigned(dwidth - 1 downto 0) := unsigned_zero;
  signal b : unsigned(dwidth - 1 downto 0) := unsigned_zero;
  signal c : unsigned(dwidth - 1 downto 0);

  signal pa : std_logic_vector(pwidth - 1 downto 0) := zero;
  signal pb : std_logic_vector(pwidth - 1 downto 0) := zero;
  signal pc : std_logic_vector(2*pwidth - 1 downto 0);

  constant patt : test_array(0 to 7) :=
    (0 => (a => 4, b => 4, c => 4, pa => 2, pb => 1, pc => 6),
     1 => (a => 5, b => 7, c => 5, pa => 2, pb => 1, pc => 2),
     2 => (a => 3, b => 1, c => 1, pa => 2, pb => 1, pc => 4),
     3 => (a => 7, b => 7, c => 7, pa => 3, pb => 3, pc => 15),
     4 => (a => 2, b => 4, c => 2, pa => 0, pb => 2, pc => 0),
     5 => (a => 0, b => 3, c => 0, pa => 0, pb => 0, pc => 0),
     6 => (a => 6, b => 2, c => 2, pa => 1, pb => 1, pc => 4),
     7 => (a => 7, b => 1, c => 1, pa => 3, pb => 3, pc => 12));

begin -- testbench

  min_cell0: min_cell
    generic map(
      dwidth => dwidth, -- data width
      pwidth => pwidth) -- pattern width
    port map(
      data0 => a,
      data1 => b,
      mdata => c,
      patt0 => pa,
      patt1 => pb,
      mpatt => pc);

  driver: process
    variable L : line;
  begin -- process
    for i in patt'range loop
      a <= to_unsigned(patt(i).a,a'LENGTH);
      b <= to_unsigned(patt(i).b,b'LENGTH);
      pa <= std_logic_vector(to_unsigned(patt(i).pa,pa'LENGTH));
      pb <= std_logic_vector(to_unsigned(patt(i).pb,pb'LENGTH));
      wait for 10 ns;
      write(L,string'("  a = ")); write(L,std_logic_vector(a));
      write(L,string'("; b = ")); write(L,std_logic_vector(b));
      write(L,string'("; c = ")); write(L,std_logic_vector(c));
      if c = to_unsigned(patt(i).c,c'LENGTH) then
        write(L,string'(" OK"));
      else
        write(L,string'(" FAILED"));
      end if;
      writeline(output,L);
      write(L,string'("  pa = ")); write(L,pa);
      write(L,string'("; pb = ")); write(L,pb);
      write(L,string'("; pc = ")); write(L,pc); 
      if pc = std_logic_vector(to_unsigned(patt(i).pc,pc'LENGTH)) then
        write(L,string'(" [ OK ]"));
      else
        write(L,string'(" [ FAILED ]"));
      end if;
      writeline(output,L);
    end loop;
    wait;
  end process driver;
  
end architecture testbench; -- of min_cell_tb
