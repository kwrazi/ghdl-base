------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Function: generic counter
-- Description:
--    Implements a binary counter that is bounded by two values. The lower
--    bounds is always zero, while the upper bounds is set by the upper
--    variable.
--    When reset is triggered, equals '1', the counter will be reset to 0 if
--    up_d_i  = '1' or 'upper' if up_d_i = '0'
--    When reset is not active, counter increments with up_d_i = '1' and
--    decrements when up_d_i = '0'. When load_i = '1', the value at the input
--    'd_i' is latched to accumulator and hence the output.

entity counter is
  generic(
    width      : natural := 8;
    lower      : natural := 0;
    upper      : natural := 255;
    halt_on_tc : boolean := false);
  port(
    clk_i  : in  std_logic;
    rst_i  : in  std_logic;
    load_i : in  std_logic; -- load data d_i
    ce_i   : in  std_logic; -- chip enable
    up_d_i : in  std_logic; -- up-down control
    d_i    : in  unsigned(width-1 downto 0);
    q_o    : out unsigned(width-1 downto 0);
    tc_o   : out std_logic);
end entity counter;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

architecture rtl of counter is

  constant lowerQ : unsigned(width-1 downto 0) := to_unsigned(lower,width);
  constant upperQ : unsigned(width-1 downto 0) := to_unsigned(upper,width);
  
  signal iQ     : unsigned(width-1 downto 0); -- internal Q
  signal startQ : unsigned(width-1 downto 0); -- start Q value
  signal finalQ : unsigned(width-1 downto 0); -- end Q value
  signal iTC    : std_logic; -- internal TC
  
begin -- rtl

  assert (lower < upper)
    report "lower value must be less then upper value."
    severity error;
  assert (upper < 2**width)
    report "upper value is out of bounds binary width."
    severity error;

  -- output
  q_o <= iQ;
  tc_o <= iTC;

  boundary_proc: process(up_d_i)
  begin -- boundary_proc
    if up_d_i = '1' then
      startQ <= lowerQ;
      finalQ <= upperQ;
    else
      startQ <= upperQ;
      finalQ <= lowerQ;
    end if;
  end process boundary_proc;
  
  tc_proc: process(iQ)
  begin -- process tc_proc
    if iQ = finalQ then
      iTC <= '1';
    else
      iTC <= '0';
    end if;
  end process tc_proc;

  core: process(clk_i,rst_i)
  begin -- process core
    if (rst_i = '1') then
      iQ <= startQ;
    elsif rising_edge(clk_i) then
      if load_i = '1' then
        iQ <= d_i;
      elsif ce_i = '1' then
        if iTC = '1' then
          if not halt_on_tc then
            iQ <= startQ;
          end if;
        else
          if up_d_i = '1' then
            iQ <= iQ + 1;
          else
            iQ <= iQ - 1;
          end if;
        end if;
      end if;
    end if;
  end process core;

end architecture rtl;
