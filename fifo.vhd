------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

-- binary counter
entity fifo_bc is
  generic (
    dwidth : integer := 32;             -- bits per word
    qdepth : integer := 16);            -- words per queue
  port (
    -- input interface
    data_i : in  std_logic_vector(dwidth-1 downto 0);
    clk_i  : in  std_logic;
    we_i   : in  std_logic;
    -- output interface
    data_o : out std_logic_vector(dwidth-1 downto 0);
    clk_o  : in  std_logic;
    re_i   : in  std_logic;
    -- flag interface
    full   : out std_logic;
    empty  : out std_logic;
    -- general interface
    rst_i  : in  std_logic);
end entity fifo_bc;

architecture rtl of fifo_bc is

  constant awidth : integer := integer(ceil(log2(real(qdepth))));

  subtype word     is std_logic_vector(dwidth-1 downto 0);
  type    fifo_mem is array (0 to qdepth-1) of word;
  subtype fifo_ptr is unsigned(awidth downto 0);  -- queue pointer

  constant zero   : fifo_ptr := (others => '0');
  constant depth  : fifo_ptr := to_unsigned(qdepth-1,awidth+1);
  
  signal fifobank : fifo_mem;
  signal ptr_i    : fifo_ptr;
  signal ptr_o    : fifo_ptr;
  signal nxtptr_i : fifo_ptr;
  signal nxtptr_o : fifo_ptr;
  signal fflag    : std_logic;             -- full flag
  signal eflag    : std_logic;             -- empty flag
  signal ptr_eq_lsw  : std_logic;             -- lsw of address equal
  signal ptr_eq_msb  : std_logic;             -- msb of address equal
 
  alias ptr_i_lsw : unsigned(awidth-1 downto 0)
    is ptr_i(awidth-1 downto 0);
  alias ptr_i_msb : std_logic is ptr_i(awidth);
  alias ptr_o_lsw : unsigned(awidth-1 downto 0)
    is ptr_o(awidth-1 downto 0);
  alias ptr_o_msb : std_logic is ptr_o(awidth);

  alias nxtptr_i_lsw : unsigned(awidth-1 downto 0)
    is nxtptr_i(awidth-1 downto 0);
  alias nxtptr_i_msb : std_logic is nxtptr_i(awidth);
  alias nxtptr_o_lsw : unsigned(awidth-1 downto 0)
    is nxtptr_o(awidth-1 downto 0);
  alias nxtptr_o_msb : std_logic is nxtptr_o(awidth);

begin -- rtl

  assert (2**awidth /= qdepth)
    report "queue depth is not a power of 2 in size."
    severity warning;

  pow2: if (2**awidth = qdepth) generate
    nxtptr_i <= ptr_i - 1;
    nxtptr_o <= ptr_o - 1;
  end generate pow2;

  notpow2: if (2**awidth /= qdepth) generate
    nxtptr_i_lsw <= depth(awidth-1 downto 0) when ptr_i_lsw = 0 else
                    ptr_i_lsw - 1;
    nxtptr_i_msb <= not ptr_i_msb when ptr_i_lsw = 0;
    nxtptr_o_lsw <= depth(awidth-1 downto 0) when ptr_o_lsw = 0 else
                    ptr_o_lsw - 1;
    nxtptr_o_msb <= not ptr_o_msb when ptr_o_lsw = 0;
  end generate notpow2;
  
  ptr_eq_lsw <= '1' when ptr_i_lsw = ptr_o_lsw else '0';
  ptr_eq_msb <= ptr_i_msb xnor ptr_o_msb;
  
  eflagunit: process (clk_o, ptr_eq_lsw)
  begin  -- process evalflag
    if ptr_eq_lsw = '1' then
      eflag <= ptr_eq_msb;
    else
      eflag <= '0';
    end if;
  end process eflagunit;
  
  fflagunit: process (clk_i, ptr_eq_lsw)
  begin  -- process evalflag
    if ptr_eq_lsw = '1' then
      fflag <= not ptr_eq_msb;
    else
      fflag <= '0';
    end if;
  end process fflagunit;
  
  -- purpose: input fifo interface
  -- type   : sequential
  -- inputs : clk_i, rst_i, we_i, data_i
  -- outputs: full
  input: process (clk_i, rst_i)
    variable addr : natural := 0;
  begin  -- process input
    if rst_i = '1' then                   -- asynchronous reset (active high)
      ptr_i <= zero;
    elsif clk_i'event and clk_i = '1' then  -- rising clock edge
      if we_i = '1' and fflag = '0' then
        addr := to_integer(ptr_i_lsw);
        fifobank(addr) <= data_i;
        ptr_i <= nxtptr_i;
      end if;
    end if;
  end process input;

  -- purpose: output fifo interface
  -- type   : sequential
  -- inputs : clk_i, rst_i, re_i
  -- outputs: empty, data_o
  output: process (clk_o, rst_i)
  begin  -- process output
    if rst_i = '1' then                   -- asynchronous reset (active high)
      ptr_o <= zero;
    elsif clk_o'event and clk_o = '1' then  -- rising clock edge
      if re_i = '1' and eflag = '0' then
        data_o <= fifobank(to_integer(ptr_o_lsw));
        ptr_o <= nxtptr_o;
      end if;
    end if;
  end process output;
  
  full <= fflag;
  empty <= eflag;
  
end architecture rtl;

---------------------------------------------------------------

-- gray code counter
entity fifo_gc is
end entity fifo_gc;

architecture rtl of fifo_gc is
begin -- rtl
end architecture rtl;
