------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;

entity file_log is
  generic(
    log_file : string  := "data.log";
    width    : natural := 32);
  port(
    clk_i  : in std_logic;
    rst_i  : in std_logic;
    data_i : in std_logic_vector(width-1 downto 0));
end entity file_log;

architecture debugging of file_log is

  file ofile: text open write_mode is log_file;
  
begin -- debugging;

  logger: process(clk_i, rst_i)
    variable L : line;
    variable prev : time := 0 ns;
    variable diff : time;
  begin -- logger
    if rst_i'event then
      diff := now - prev;
      write(L, time'image(diff));
      write(L, ' ');
      write(L, data_i);
      writeline(ofile,L);
      prev := now;
    elsif rising_edge(clk_i) then
      if rst_i = '0' then
        diff := now - prev;
        write(L, time'image(diff));
        write(L, ' ');
        write(L, data_i);
        writeline(ofile,L);
        prev := now;
      end if;
    end if;
  end process logger;
  
end architecture debugging;

-----------------------------------------------------------------

library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;

entity file_src is
  generic(
    src_file : string  := "data.log";
    width    : natural := 32);
  port(
    data_o : out std_logic_vector(width-1 downto 0));
end entity file_src;

architecture debugging of file_src is

  file ifile: text open read_mode is src_file;
  
begin -- debugging;

  reader: process
    variable L     : line;
    variable wtime : time;
    variable space : character;
    variable bdata : std_logic_vector(width-1 downto 0);
  begin -- reader
    while not endfile(ifile) loop
      readline(ifile, L);
      read(L, wtime);
      read(L, space);
      read(L, bdata);
      data_o <= bdata;
      wait for wtime;
    end loop;
    wait;
  end process reader;
  
end architecture debugging;

-----------------------------------------------------------------

library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;

entity file_cmp is
  generic(
    src_file : string  := "data.log";
    width    : natural := 32);
  port(
    clk_i  : in std_logic;
    rst_i  : in std_logic;
    data_i : in  std_logic_vector(width-1 downto 0);
    data_o : out std_logic_vector(width-1 downto 0));
end entity file_cmp;

architecture debugging of file_cmp is

  file ifile: text open read_mode is src_file;

  signal bdata : std_logic_vector(width-1 downto 0);
  
begin -- debugging;

  data_o <= bdata;
  
  reader: process
    variable L     : line;
    variable wtime : time;
    variable space : character;
    variable tmp   : std_logic_vector(width-1 downto 0);
  begin -- reader
    while not endfile(ifile) loop
      readline(ifile, L);
      read(L, wtime);
      read(L, space);
      read(L, tmp);
      bdata <= tmp;
      wait for wtime;
    end loop;
    wait;
  end process reader;

  compare: process(clk_i, rst_i)
    variable L : line;
  begin -- compare
    if falling_edge(clk_i) then
      if rst_i = '0' then
        if bdata /= data_i then
          -- report "mismatch" severity warning;
          write(L, time'image(now));
          write(L, string'(": input=>"));
          hwrite(L,data_i);
          write(L, string'("; expected=>"));
          hwrite(L,bdata);
          write(L, string'("; mismatch"));
          writeline(output,L);
        end if;
      end if;
    end if;
  end process compare;
  
end architecture debugging;
