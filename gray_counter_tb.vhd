------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------
-- Code based on Ing. Ivo Viscor 2000 paper, "Gray counter in VHDL"

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;
use ieee.math_real.all;

entity gray_counter_tb is
end entity gray_counter_tb;

use work.graycode_lib.all;

architecture testbench of gray_counter_tb is

  -- component generics settings
  constant width : natural := 5;
  
  -- Specifies which entity is bound with the component.
  for gray_counter_0: gray_counter use entity work.gray_counter;
  for gray_to_binary_0: gray_to_binary use entity work.gray_to_binary;
  for binary_to_gray_0: binary_to_gray use entity work.binary_to_gray;
  for gray_counter_asym_0: gray_counter_asym use entity work.gray_counter_asym;

  -- Wires to connect test component
  signal reset, clock : std_logic;
  signal parity0 : std_logic;
  signal parity1 : std_logic;
  signal code0 : std_logic_vector(width-1 downto 0);
  signal code1 : std_logic_vector(width-1 downto 0);
  signal code2 : std_logic_vector(width-1 downto 0);
  signal code3 : std_logic_vector(width-1 downto 0);
  
  constant clk_period: time := 5 ns;
  
begin --testbench

  -- Component instantiation.
  gray_counter_0: gray_counter
  generic map (
    width => width)
  port map (
    rst_i => reset,
    clk_i => clock,
    p_io  => parity0,
    q_io  => code0);

  gray_to_binary_0: gray_to_binary
  generic map (
    width => width)
  port map (
    g_i => code0,
    b_o => code1);

  binary_to_gray_0: binary_to_gray
  generic map (
    width => width)
  port map (
    b_i => code1,
    g_o => code2);

  gray_counter_asym_0: gray_counter_asym
  generic map (
    width => width)
  port map (
    rst_i => reset,
    clk_i => clock,
    p_io  => parity1,
    q_io  => code3);

  reset_process: process
  begin
    reset <= '1';
    wait for clk_period * 5;
    reset <= '0';
    wait;
  end process reset_process;
  
  -- Clock to infinity
  clock_process: process
  begin
    clock <= '0';
    wait for clk_period/2;
    clock <= '1';
    wait for clk_period/2;
  end process clock_process;

end architecture testbench;
