------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

entity counter_tb is
end entity counter_tb;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.component_lib.all;

architecture testbench of counter_tb is

  constant lower : natural := 0;
  constant upper : natural := 15;
  constant width : natural := 6;
  
  for all : clock_gen use entity work.clock_gen(behaviour);
  for all : counter use entity work.counter(rtl);
  
  signal clk    : std_logic;
  signal rst    : std_logic;
  signal tc     : std_logic;
  signal count  : unsigned(width-1 downto 0);
  
begin --testbench

  clk_gen0: clock_gen
    generic map (
      finite    => true,
      clk_start => '0',
      period    => 10 ns,
      clk_tick  => 100,
      rst_tick  => 5.0)
    port map (
      clk_o     => clk,
      rst_o     => rst);

  cnt: counter
    generic map (
      width      => width,
      lower      => lower,
      upper      => upper,
      halt_on_tc => false)
    port map (
      clk_i  => clk,
      rst_i  => rst,
      load_i => '0',
      ce_i   => '1',
      up_d_i => '1',
      d_i    => to_unsigned(lower,width),
      q_o    => count,
      tc_o   => tc);
  
end architecture testbench;
