##############################################################
# Makefile for gvhdl
#
# Kiet To - October 2013
#
##############################################################

     SRCDIR = .
       SRCS = $(shell ls -1 $(SRCDIR)/*.vhd 2> /dev/null)
       OBJS = $(SRCS:.vhd=.o)
    WORKDIR = work
     SIMDIR = work

    HDL_CMD = ghdl
  HDL_FLAGS = --std=93c --ieee=synopsys --workdir=$(WORKDIR)
    ANA_OPT = --warn-no-vital-generic $(HDL_FLAGS)
    EXE_OPT = $(HDL_FLAGS)
    RUN_OPT = $(HDL_FLAGS)
    SIM_OPT = --stop-time=1ms
   VIEW_CMD = gtkwave

# ---------------------------------------------------------------
define HELP_TEXT
Make options
  no argument     - compile vhd files only.
  clean           - clean work directory of objects
  distclean       - distribution clean
  list            - list entities, packages and architectures
  <toplevel>.exe  - create executable of toplevel entity
  <toplevel>.run  - create executable and run simulation
  <toplevel>.view - create executable, run simulation, open vcd viewer
endef

export HELP_TEXT
# ---------------------------------------------------------------

# intermediate files not to delete
.PRECIOUS:	$(WORKDIR)/%.o %.vcd

# ---------------------------------------------------------------
all:	setup import compile

setup:	
	@if [ ! -d $(WORKDIR) ]; then \
		mkdir -p $(WORKDIR); \
	fi

import:	
	@echo "Importing vhdl files..."
	@$(HDL_CMD) -i --workdir=$(WORKDIR) $(SRCS)

compile: $(OBJS)

list:
	@if [ -f $(WORKDIR)/*.cf ]; then \
		grep " at " $(WORKDIR)/*.cf | sed -e 's/ at .*$$//' | sed -e 's/ \(entity\|architecture\|of\|package\|package body\) / \U\1 /g'; \
	else \
		echo "No work library."; \
	fi

clean:
	@$(HDL_CMD) --clean $(HDL_FLAGS)
	@rm -f *.vcd $(SRCDIR)/*~ *~

distclean:	clean
	@rm -rf $(WORKDIR)

help:	
	@echo "$$HELP_TEXT"

# ---------------------------------------------------------------

%.view:	%.vcd
	$(VIEW_CMD) $<

%.vcd:	%.run
	@echo "Entity $(<:.run=) executed."

%.run:	%.exe
	$(HDL_CMD) -r $(RUN_OPT) $(@:.run=) --vcd=$(@:.run=.vcd) $(SIM_OPT)

%.exe:	setup import $(OBJS)
	$(HDL_CMD) -m $(EXE_OPT) $(@:.exe=)

# OBJS rules
$(SRCDIR)/%.o:	$(WORKDIR)/%.o
	@echo "Object $< built."

# VHDL compile rules
$(WORKDIR)/%.o:	$(SRCDIR)/%.vhd
	$(HDL_CMD) -a $(ANA_OPT) $<
