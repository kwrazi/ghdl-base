------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-- Fibonacci LFSR
entity lfsr is
  generic(
    width  : natural := 32);
  port(
    clk_i  : in  std_logic;
    rst_i  : in  std_logic;
    load_i : in  std_logic; -- load data d_i
    ce_i   : in  std_logic; -- chip enable
    seed_i : in  std_logic_vector(width-1 downto 0);
    q_o    : out std_logic_vector(width-1 downto 0));
end entity lfsr;

library ieee;
use ieee.std_logic_1164.all;
use work.lfsr_pkg.all;

architecture rtl of lfsr is

  constant reset_seed : std_logic_vector(width downto 1) :=
    ('1', others => '0');
  constant tapcode : natural := find_taps(width);
  constant t : taps_list_type := maximal_length_table(tapcode);
  
  signal iLFSR : std_logic_vector(width downto 0);
  
begin -- rtl

  assert (tapcode > 0)
    report "maximal length table does not contain taps for width specified."
    severity error;
  
  q_o <= iLFSR(width downto 1);

  iLFSR(0) <= '0';

  core: process(clk_i,rst_i)
  begin -- core
    if rst_i = '1' then
      iLFSR(width downto 1) <= reset_seed;
    elsif rising_edge(clk_i) then
      if ce_i = '1' then
        if load_i = '1' then
          iLFSR(width downto 1) <= seed_i;
        else
          iLFSR(1) <= iLFSR(t(0)) xor iLFSR(t(1)) xor
                      iLFSR(t(2)) xor iLFSR(t(3));
          iLFSR(width downto 2) <= iLFSR(width-1 downto 1);
        end if;
      end if;
    end if;
  end process core;
  
end architecture rtl;

------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity lfsr_galois is
  generic(
    width  : natural := 32);
  port(
    clk_i  : in  std_logic;
    rst_i  : in  std_logic;
    load_i : in  std_logic; -- load data d_i
    ce_i   : in  std_logic; -- chip enable
    seed_i : in  std_logic_vector(width-1 downto 0);
    q_o    : out std_logic_vector(width-1 downto 0));
end entity lfsr_galois;

use work.lfsr_pkg.all;

architecture rtl of lfsr_galois is
  
  constant reset_seed : std_logic_vector(width-1 downto 0) :=
    (0 => '1', others => '0');
  
  constant tapcode : natural := find_taps(width);
  constant t : taps_list_type := maximal_length_table(tapcode);
  
  signal iLFSR : std_logic_vector(width downto 0) := (others => '0');
  
begin -- rtl

  assert (tapcode > 0)
    report "maximal length table does not contain taps for width specified."
    severity error;
  
  q_o <= iLFSR(width-1 downto 0);
  
  iLFSR(width) <= '0';

  core: process(clk_i,rst_i)
    variable tap : natural;
  begin -- core
    if rst_i = '1' then
      iLFSR(width) <= '0';
      iLFSR(width-1 downto 0) <= reset_seed;
    elsif rising_edge(clk_i) then
      if ce_i = '1' then
        if load_i = '1' then
          iLFSR(width-1 downto 0) <= seed_i;
        else
          -- find first non-zero tap
          for i in taps_list_type'reverse_range loop
            if t(i) > 0 then
              tap := i;
              exit;
            end if;
          end loop;
          -- shift and xor
          for i in 0 to width-1 loop
            if i = (t(tap)-1) then
              if tap > 0 then
                tap := tap - 1;
              end if;
              iLFSR(i) <= iLFSR(i+1) xor iLFSR(0);
            else
              iLFSR(i) <= iLFSR(i+1);
            end if;
          end loop;
        end if;
      end if;
    end if;
  end process core;

end architecture rtl;
