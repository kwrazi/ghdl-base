------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package component_lib is
  
  component clock_gen is
    generic (
      finite    : boolean := true;
      clk_start : std_logic := '1';
      period    : time    := 10 ns;
      clk_tick  : natural := 100;
      rst_tick  : real    := 5.0;
      clk_delay : real    := 0.0);
    port (
      clk_o : out std_logic;
      rst_o : out std_logic);
  end component clock_gen;
  
  component counter is
    generic(
      width      : natural := 8;
      lower      : natural := 0;
      upper      : natural := 255;
      halt_on_tc : boolean := false);
    port(
      clk_i  : in  std_logic;
      rst_i  : in  std_logic;
      load_i : in  std_logic; -- load data d_i
      ce_i   : in  std_logic; -- chip enable
      up_d_i : in  std_logic; -- up-down control
      d_i    : in  unsigned(width-1 downto 0);
      q_o    : out unsigned(width-1 downto 0);
      tc_o   : out std_logic);
  end component counter;

  component lfsr is
    generic(
      width  : natural := 32);
    port(
      clk_i  : in  std_logic;
      rst_i  : in  std_logic;
      load_i : in  std_logic; -- load data d_i
      ce_i   : in  std_logic; -- chip enable
      seed_i : in  std_logic_vector(width-1 downto 0);
      q_o    : out std_logic_vector(width-1 downto 0));
  end component lfsr;

  component lfsr_galois is
    generic(
      width  : natural := 32);
    port(
      clk_i  : in  std_logic;
      rst_i  : in  std_logic;
      load_i : in  std_logic; -- load data d_i
      ce_i   : in  std_logic; -- chip enable
      seed_i : in  std_logic_vector(width-1 downto 0);
      q_o    : out std_logic_vector(width-1 downto 0));
  end component lfsr_galois;
  
  component file_log is
    generic(
      log_file : string  := "data.log";
      width    : natural := 32);
    port(
      clk_i  : in std_logic;
      rst_i  : in std_logic;
      data_i : in std_logic_vector(width-1 downto 0));
  end component file_log;
  
  component file_src is
    generic(
      src_file : string  := "data.log";
      width    : natural := 32);
    port(
      data_o : out std_logic_vector(width-1 downto 0));
  end component file_src;
  
  component file_cmp is
    generic(
      src_file : string  := "data.log";
      width    : natural := 32);
    port(
      clk_i  : in std_logic;
      rst_i  : in std_logic;
      data_i : in  std_logic_vector(width-1 downto 0);
      data_o : out std_logic_vector(width-1 downto 0));
  end component file_cmp;
  
  component fifo_bc is
    generic (
      dwidth : integer := 32;
      qdepth : integer := 16);
    port (
      data_i : in  std_logic_vector(dwidth-1 downto 0);
      clk_i  : in  std_logic;
      we_i   : in  std_logic;
      data_o : out std_logic_vector(dwidth-1 downto 0);
      clk_o  : in  std_logic;
      re_i   : in  std_logic;
      full   : out std_logic;
      empty  : out std_logic;
      rst_i  : in  std_logic);
  end component fifo_bc;

end package component_lib;
