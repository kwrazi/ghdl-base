------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use ieee.math_real.all;

use std.textio.all;

use work.slice_pkg.all;

entity min_tree is
  generic (
    width : natural := 4;
    depth : natural := 3);
  port (
    inputs   : in  std_logic_vector((2**depth * width - 1) downto 0);
    minvalue : out unsigned(width-1 downto 0);
    pos      : out std_logic_vector(2**depth - 1 downto 0));
end entity min_tree;


architecture logic of min_tree is

  constant btree_nodes : natural := 2**(depth+1)-1;
  constant btree_width : natural := 2**(depth);

  constant dwidth : natural := btree_width;

  component min_cell is
    generic (
      dwidth : natural;
      pwidth : natural);
    port (
      data0  : in  unsigned(dwidth - 1 downto 0);
      data1  : in  unsigned(dwidth - 1 downto 0);
      mdata  : out unsigned(dwidth - 1 downto 0);
      patt0  : in  std_logic_vector(pwidth - 1 downto 0);
      patt1  : in  std_logic_vector(pwidth - 1 downto 0);
      mpatt  : out std_logic_vector(2*pwidth - 1 downto 0));
  end component;
  
  subtype value   is unsigned(width-1 downto 0);
  subtype pattern is std_logic_vector(dwidth-1 downto 0);
  type valuelist  is array(0 to 2*dwidth-2) of value;
  type pattlist   is array(0 to depth) of pattern;

  -- for minimizer: min_cell use entity work.min_cell;
  
  signal valuetree : valuelist;
  signal patttree  : pattlist;

begin -- logic of min_tree

  -- inputs connections
  patttree(0) <= (others => '1');
  input_stage: for i in integer(btree_width-1) downto 0 generate
    valuetree(i) <= unsigned_read_slice(inputs,width,i);
  end generate input_stage;

  stages: for i in 0 to integer(depth-1) generate
    subtype ipatt is std_logic_vector(2**i - 1 downto 0);
    subtype opatt is std_logic_vector(2*2**i - 1 downto 0);

    type opatt_array is array(0 to integer(2**(depth-i-1) - 1)) of opatt;

    signal op : opatt_array;
  begin -- stages

    row_minimizers : for j in 0 to integer(2**(depth-i-1) - 1) generate
      constant a : natural := btree_to_linear(depth+1,i,2*j);
      constant b : natural := btree_to_linear(depth+1,i,2*j+1);
      constant c : natural := btree_to_linear(depth+1,i+1,j);
      
      signal x,y : ipatt;
      signal z   : opatt;
      
    begin -- row_minimizers

      x <= slv_read_slice(patttree(i),x'LENGTH,2*j);
      y <= slv_read_slice(patttree(i),y'LENGTH,2*j+1);

      minimizer : min_cell
        generic map (
          dwidth => width,
          pwidth => 2**i)
        port map (
          -- data values
          data0 => valuetree(a),
          data1 => valuetree(b),
          mdata => valuetree(c),
          -- minimum bit mask pattern
          patt0 => x,
          patt1 => y,
          mpatt => z);
      
      op(j) <= z;
      
    end generate row_minimizers;

    pattwriter: process(op)
      variable next_stage : pattern;
    begin -- pattwritter
      for i in op'range loop
        slv_insert_slice(next_stage,op(i),i);
      end loop;
      patttree(i+1) <= next_stage;
    end process pattwriter;

  end generate stages;

  -- output connections
  minvalue <= unsigned(valuetree(2*dwidth-2));
  pos <= patttree(depth);
  
end architecture logic; -- of min_tree
