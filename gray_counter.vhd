------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------
-- Code based on Ing. Ivo Viscor 2000 paper, "Gray counter in VHDL"

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;
use ieee.math_real.all;

entity gray_1 is
  generic(
    reset_value : std_logic := '0');
  port(
    rst_i : in    std_logic;
    clk_i : in    std_logic;
    q_i   : in    std_logic;
    z_i   : in    std_logic;
    q_o   : inout std_logic;
    z_o   : out   std_logic);
end gray_1;

architecture rtl of gray_1 is
begin -- rtl

  process(rst_i, clk_i)
  begin
    if rst_i = '1' then
      q_o <= reset_value;
    elsif rising_edge(clk_i) then
      q_o <= q_o xor (q_i and z_i);
    end if;
  end process;

  z_o <= z_i and not q_i;
  
end architecture rtl;

------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
--  BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--  LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------
-- Code based on Ing. Ivo Viscor 2000 paper, "Gray counter in VHDL"

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;
use ieee.math_real.all;

entity gray_counter is
  generic(
    width: natural := 4);
  port(
    rst_i : in    std_logic;
    clk_i : in    std_logic;
    p_io  : inout std_logic;   -- parity bit
    q_io  : inout std_logic_vector(width-1 downto 0));
end entity gray_counter;

use work.graycode_lib.all;

architecture rtl of gray_counter is

  signal z  : std_logic_vector(width-1 downto 0);
  signal qx : std_logic;
  
begin -- rtl

  assert width > 1
    report "Gray code bit width must be two than greater."
    severity FAILURE;
  
  create_lsb: gray_1
    generic map ('0')
    port map (rst_i, clk_i, p_io, '1', q_io(0), z(0));

  two_or_more: if width > 1 generate
    three_or_more: if width > 2 generate
      create_mid: for i in 1 to width-2 generate
        createbit: gray_1
          generic map ('0')
          port map (rst_i, clk_i, q_io(i-1), z(i-1), q_io(i), z(i));
      end generate;
    end generate;

    qx <= q_io(width-2) or q_io(width-1);

    create_msb: gray_1
      generic map ('0')
      port map (rst_i, clk_i, qx, z(width-2), q_io(width-1), z(width-1));
    
  end generate;

  parity_bit: process(rst_i, clk_i)
  begin -- parity bit
    if rst_i = '1' then
      p_io <= '1';
    elsif rising_edge(clk_i) then
      p_io <= not p_io;
    end if;
  end process parity_bit;
  
end architecture rtl;

------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
--  BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--  LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------
-- based on http://electronicsinourhands.blogspot.com.au/2012/11/binary-to-gray-and-gray-to-binary.html

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;
use ieee.math_real.all;

entity gray_to_binary is
  generic(
    width: natural := 8);
  port(
    g_i  : in  std_logic_vector(width-1 downto 0);
    b_o  : out std_logic_vector(width-1 downto 0));
end entity gray_to_binary;

architecture rtl of gray_to_binary is

  signal x : std_logic_vector(width-1 downto 0);
  
begin
  
  assert width > 1
    report "Gray code bit width must be two than greater."
    severity FAILURE;

  x(width-1) <= g_i(width-1);

  convertor: for i in width-2 downto 0 generate
    x(i) <= x(i+1) xor g_i(i);
  end generate;

  b_o <= x;
  
end architecture rtl;

------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
--  BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--  LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------
-- based on http://electronicsinourhands.blogspot.com.au/2012/11/binary-to-gray-and-gray-to-binary.html

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;
use ieee.math_real.all;

entity binary_to_gray is
  generic(
    width: natural := 8);
  port(
    b_i  : in  std_logic_vector(width-1 downto 0);
    g_o  : out std_logic_vector(width-1 downto 0));
end entity binary_to_gray;

architecture rtl of binary_to_gray is

begin
  
  assert width > 1
    report "Gray code bit width must be two than greater."
    severity FAILURE;

  g_o(width-1) <= b_i(width-1);
  
  convertor: for i in 0 to width-2 generate
    g_o(i) <= b_i(i+1) xor b_i(i);
  end generate;
  
end architecture rtl;

------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
--  BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--  LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_textio.all;
use ieee.math_real.all;

entity gray_counter_asym is
  
  generic(
    width: natural := 4);
  port(
    rst_i : in    std_logic;
    clk_i : in    std_logic;
    p_io  : inout std_logic;   -- parity bit
    q_io  : inout std_logic_vector(width-1 downto 0));
end entity gray_counter_asym;

use work.graycode_lib.all;

architecture rtl of gray_counter_asym is

  constant gray_width : natural := width-1;

  signal z  : std_logic_vector(gray_width-1 downto 0);
  signal qx : std_logic;

begin -- rtl

  assert gray_width > 1
    report "Asymmetric gray code bit width must be three than greater."
    severity FAILURE;
  
  create_lsb: gray_1
    generic map ('0')
    port map (rst_i, clk_i, p_io, '1', q_io(0), z(0));

  two_or_more: if gray_width > 1 generate
    three_or_more: if gray_width > 2 generate
      create_mid: for i in 1 to gray_width-2 generate
        createbit: gray_1
          generic map ('0')
          port map (rst_i, clk_i, q_io(i-1), z(i-1), q_io(i), z(i));
      end generate;
    end generate;

    qx <= q_io(gray_width-2) or q_io(gray_width-1);

    create_msb: gray_1
      generic map ('0')
      port map (rst_i, clk_i, qx, z(gray_width-2), q_io(gray_width-1), z(gray_width-1));
    
  end generate;

  parity_bit: process(rst_i, clk_i)
  begin -- parity bit
    if rst_i = '1' then
      p_io <= '1';
    elsif rising_edge(clk_i) then
      p_io <= not p_io;
    end if;
  end process parity_bit;

  msb_bit: process(rst_i, qx)
  begin -- msb_bit
    if rst_i = '1' then
      q_io(width-1) <= '0';
    elsif falling_edge(qx) then
      q_io(width-1) <= not q_io(width-1);
    end if;
  end process msb_bit;
  
end architecture rtl;
