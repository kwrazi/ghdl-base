------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity lfsr_tb is
end entity lfsr_tb;

library std;
use std.textio.all;
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use work.component_lib.all;

architecture testbench of lfsr_tb is

  constant width : natural := 32;
  constant ticks : natural := 200;

  for all : clock_gen use entity work.clock_gen(behaviour);
  for all : lfsr use entity work.lfsr(rtl);
  for all : lfsr_galois use entity work.lfsr_galois(rtl);

  signal clk     : std_logic;
  signal rst     : std_logic;
  signal seed    : std_logic_vector(width-1 downto 0);
  signal rndout1 : std_logic_vector(width-1 downto 0);
  signal rndout2 : std_logic_vector(width-1 downto 0);
  
begin -- testbench

  seed <= (width-1 => '1', others => '0');
  
  clk_gen0: clock_gen
    generic map (
      finite    => true,
      clk_start => '0',
      period    => 10 ns,
      clk_tick  => ticks,
      rst_tick  => 5.0)
    port map (
      clk_o     => clk,
      rst_o     => rst);

  prbg1: lfsr
    generic map (
      width  => width)
    port map(
      clk_i  => clk,
      rst_i  => rst,
      load_i => '0',
      ce_i   => '1',
      seed_i => seed,
      q_o    => rndout1);
  
  prbg2: lfsr_galois
    generic map (
      width  => width)
    port map(
      clk_i  => clk,
      rst_i  => rst,
      load_i => '0',
      ce_i   => '1',
      seed_i => seed,
      q_o    => rndout2);

  debug: process(clk,rst)
    variable L : line;
  begin -- debug
    if rising_edge(clk) then
      if rst = '0' then
        -- debugging
        write(L, time'image(now));
        write(L, string'(": output1 => "));
        hwrite(L, rndout1);
        write(L, string'("; output2 => "));
        hwrite(L, rndout2);
        writeline(output, L);
      end if;
    end if;
  end process debug;
  
end architecture testbench;
