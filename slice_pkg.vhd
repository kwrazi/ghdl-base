------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;

use std.textio.all;

package slice_pkg is

  constant DEBUG_SLICE_PKG : boolean := false;

  -- linear slice
  function slv_read_slice(data  : std_logic_vector;
                          width : natural;
                          index : natural) return std_logic_vector;
  
  function unsigned_read_slice(data  : std_logic_vector;
                               width : natural;
                               index : natural) return unsigned;
  
  procedure slv_insert_slice(variable data  : inout std_logic_vector;
                             constant slice : in    std_logic_vector;
                             constant index : in    natural);

  procedure slv_insert_slice_sig(signal   data  : inout std_logic_vector;
                                 constant slice : in    std_logic_vector;
                                 constant index : in    natural);

  -- binary tree slice
  function btree_to_linear(constant depth : natural;
                           constant row   : natural;
                           constant col   : natural) return natural;
  function linear_to_btreerow(depth : natural;
                              index : natural) return natural;
  function linear_to_btreecol(depth : natural;
                              index : natural) return natural;
  function slv_read_btree(data   : std_logic_vector;
                          width  : natural;
                          depth  : natural;
                          row    : natural;
                          col    : natural) return std_logic_vector;
  function unsigned_read_btree(data   : std_logic_vector;
                               width  : natural;
                               depth  : natural;
                               row    : natural;
                               col    : natural) return unsigned;
  procedure slv_insert_btree(variable data  : inout std_logic_vector;
                             constant slice : in    std_logic_vector;
                             constant depth : in    natural;
                             constant row   : in    natural;
                             constant col   : in    natural);

  procedure slice_write(L : inout line;
                        data : in std_logic_vector;
                        width : in natural);
end package;

package body slice_pkg is

  -- linear slice
  function slv_read_slice(data  : std_logic_vector;
                          width : natural;
                          index : natural) return std_logic_vector is
    constant dwidth   : natural := data'length;
    constant slicenum : natural := natural(dwidth / width);
    variable result   : std_logic_vector(width-1 downto 0);
    variable L        : line;
  begin
    if DEBUG_SLICE_PKG then
      write(L,string'("slv_read_slice("));
      write(L,data); write(L,',');
      write(L,width); write(L,',');
      write(L,index); write(L,string'("):result="));
    end if;
    assert width < dwidth
      report "width is not less then dwidth."
      severity error;
    assert (dwidth mod width) = 0
      report "dwidth is not divisible by width."
      severity error;
    assert index < slicenum
      report "index is out of range of number of slices."
      severity error;

    result := data(((index + 1)*width - 1) downto index*width);
    if DEBUG_SLICE_PKG then
      write(L,result);
      writeline(output,L);
    end if;
    return result;
  end function slv_read_slice;

  function unsigned_read_slice(data  : std_logic_vector;
                               width : natural;
                               index : natural) return unsigned is
  begin
    return unsigned(slv_read_slice(data,width,index));
  end function unsigned_read_slice;
  
  procedure slv_insert_slice(variable data  : inout std_logic_vector;
                             constant slice : in    std_logic_vector;
                             constant index : in    natural) is
    constant dwidth   : natural := data'length;
    constant width    : natural := slice'length;
    constant slicenum : natural := natural(dwidth / width);
    variable L        : line;
  begin
    if DEBUG_SLICE_PKG then
      write(L,string'("slv_insert_slice("));
      write(L,data); write(L,',');
      write(L,slice); write(L,',');
      write(L,index); write(L,')');
      writeline(output,L);
    end if;
    assert width <= dwidth
                    report "width is not less then dwidth."
                    severity error;
    assert (dwidth mod width) = 0
      report "dwidth is not divisible by width."
      severity error;
    assert index < slicenum
      report "index is out of range of number of slices."
      severity error;

    data(((index + 1)*width - 1) downto index*width) := slice;

  end procedure slv_insert_slice;

  procedure slv_insert_slice_sig(signal data  : inout std_logic_vector;
                             constant slice : in    std_logic_vector;
                             constant index : in    natural) is
    constant dwidth   : natural := data'length;
    constant width    : natural := slice'length;
    constant slicenum : natural := natural(dwidth / width);
    variable L        : line;
  begin
    if DEBUG_SLICE_PKG then
      write(L,string'("slv_insert_slice("));
      write(L,data); write(L,',');
      write(L,slice); write(L,',');
      write(L,index); write(L,')');
      writeline(output,L);
    end if;
    assert width <= dwidth
                    report "width is not less then dwidth."
                    severity error;
    assert (dwidth mod width) = 0
      report "dwidth is not divisible by width."
      severity error;
    assert index < slicenum
      report "index is out of range of number of slices."
      severity error;

    data(((index + 1)*width - 1) downto index*width) <= slice;

  end procedure slv_insert_slice_sig;
  
  -- binary tree slice
  function btree_to_linear(constant depth : natural;
                           constant row   : natural;
                           constant col   : natural) return natural is
    constant colwidth : natural := 2**(depth - row - 1);
    constant offset   : natural := 2**depth - 2**(depth-row);
    constant index    : natural := offset + col;
    variable L : line;
  begin
    if DEBUG_SLICE_PKG then
      write(L,string'("btree_to_linear("));
      write(L,depth); write(L,',');
      write(L,row); write(L,',');
      write(L,col); write(L,')');
      writeline(output,L);
    end if;
    assert row < depth
      report "row index must be less then depth."
      severity error;
    assert col < colwidth
      report "col index is out of range, given row."
      severity error;
    return index;
  end function btree_to_linear;

  function linear_to_btreerow(depth : natural;
                              index : natural) return natural is
    variable row      : natural;
    variable rowrange : natural;
  begin
    for row in 0 to depth-1 loop
      rowrange := 2**depth - 2**(depth-row-1);
      if index < rowrange then
        return row;
      end if;
    end loop;
    assert false
      report "index out of range."
      severity error;
  end function linear_to_btreerow;
  
  function linear_to_btreecol(depth : natural;
                              index : natural) return natural is
    variable row      : natural;
    variable colwidth : natural;
    variable tmp      : natural := index;
  begin
    for row in 0 to depth-1 loop
      colwidth := 2**(depth - row - 1);
      if tmp < colwidth then
        return tmp;
      else
        tmp := tmp - colwidth;
      end if;
    end loop;
    assert false
      report "index out of range."
      severity error;
  end function linear_to_btreecol;

  function slv_read_btree(data   : std_logic_vector;
                          width  : natural;
                          depth  : natural;
                          row    : natural;
                          col    : natural) return std_logic_vector is
    constant index    : natural := btree_to_linear(depth,row,col);
    variable result   : std_logic_vector(width-1 downto 0);
  begin
    result := slv_read_slice(data,width,index);
    return result;
  end function slv_read_btree;

  function unsigned_read_btree(data   : std_logic_vector;
                               width  : natural;
                               depth  : natural;
                               row    : natural;
                               col    : natural) return unsigned is
  begin
    return unsigned(slv_read_btree(data,width,depth,row,col));
  end function unsigned_read_btree;

  procedure slv_insert_btree(variable data  : inout std_logic_vector;
                             constant slice : in    std_logic_vector;
                             constant depth : in    natural;
                             constant row   : in    natural;
                             constant col   : in    natural) is
    constant width    : natural := slice'length;
    constant index    : natural := btree_to_linear(depth,row,col);
  begin
    slv_insert_slice(data,slice,index);
  end procedure slv_insert_btree;
  
  procedure slice_write(
    L : inout line;
    data : in std_logic_vector;
    width : in natural) is
    constant dwidth : natural := data'LENGTH;
    constant slices : natural := dwidth / width;
    variable slice : std_logic_vector(width-1 downto 0);
  begin
    assert width <= dwidth
      report "width is not less then dwidth."
      severity error;
    assert (dwidth mod width) = 0
      report "dwidth is not divisible by width."
      severity error;
    for i in slices-1 downto 0 loop
      slice := data(((i + 1)*width - 1) downto i*width);
      write(L,slice);
      write(L,' ');
    end loop;
  end procedure slice_write;
  
end package body;
