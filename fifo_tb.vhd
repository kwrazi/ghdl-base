------------------------------------------------------------------------
--
--  Copyright (C) 2013 Kiet To (kwrazi@gmail.com)
-- 
--  Donation:
--    BitCoin:  18QTRqT3xzHGnbqL8bsPkZNQPDm1DqhNz6
--    LiteCoin: LPPA4RaEDPtae98z634sgHoMy5fp1aoJk6
--
------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;

entity fifo_tb is
end entity fifo_tb;

use work.component_lib.all;

architecture testbench of fifo_tb is


  constant width : natural := 16;
  constant ticks : natural := 64;
  
  for all : fifo_bc use entity work.fifo_bc(rtl);

  subtype word is std_logic_vector(width-1 downto 0);

  signal clk     : std_logic;
  signal rst     : std_logic;
  signal data    : word;
  signal seed    : word;
  
  signal clk_1   : std_logic;
  signal rst_1   : std_logic;
  signal re_1    : std_logic;
  signal data_1  : word;
  signal full_1  : std_logic;
  signal empty_1 : std_logic;

  signal clk_2   : std_logic;
  signal rst_2   : std_logic;
  signal re_2    : std_logic;
  signal data_2  : word;
  signal full_2  : std_logic;
  signal empty_2 : std_logic;
  
begin -- testbench

  seed <= (width-1 => '1', others => '0');
  re_1 <= not rst_1;
  re_2 <= not rst_2;
  
  clk_gen0: clock_gen
    generic map (
      finite    => true,
      clk_start => '0',
      period    => 10 ns,
      clk_tick  => ticks,
      rst_tick  => 5.0)
    port map (
      clk_o     => clk,
      rst_o     => rst);

  clk_gen1: clock_gen
    generic map (
      finite    => true,
      clk_start => '0',
      period    => 10 ns,
      clk_tick  => ticks*2,
      rst_tick  => 37.0)
    port map (
      clk_o     => clk_1,
      rst_o     => rst_1);
  
  clk_gen2: clock_gen
    generic map (
      finite    => true,
      clk_start => '0',
      period    => 10 ns,
      clk_tick  => ticks*2,
      rst_tick  => 37.0)
    port map (
      clk_o     => clk_2,
      rst_o     => rst_2);
  
  prbg1: lfsr_galois
    generic map (
      width  => width)
    port map(
      clk_i  => clk,
      rst_i  => rst,
      load_i => '0',
      ce_i   => '1',
      seed_i => seed,
      q_o    => data);    
  
  fifo1: fifo_bc
    generic map (
      dwidth => width,
      qdepth => 10)
    port map (
      data_i => data,
      clk_i  => clk,
      we_i   => '1',
      data_o => data_1,
      clk_o  => clk_1,
      re_i   => re_1,
      full   => full_1,
      empty  => empty_1,
      rst_i  => rst);
    
  fifo2: fifo_bc
    generic map (
      dwidth => width,
      qdepth => 16)
    port map (
      data_i => data,
      clk_i  => clk,
      we_i   => '1',
      data_o => data_2,
      clk_o  => clk_2,
      re_i   => re_2,
      full   => full_2,
      empty  => empty_2,
      rst_i  => rst);

end architecture testbench;
